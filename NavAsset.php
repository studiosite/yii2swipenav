<?php

namespace studiosite\yii2swipenav;

use Yii;
use yii\web\AssetBundle;

/**
 * Бадл Nav
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class NavAsset extends AssetBundle
{
	/**
    * @var string Альяс пути где находятся асетсы
    */
	public $baseUrl = '@web';

	/**
    * @var array Исходный путь
    */
    public $sourcePath = '@yii2swipenav/assets';

	/**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
        'style.scss'
    ];

	/**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
        'script.js'
    ];

    /**
    * @var array Список асетсов - зависимости текущего асетса
    */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        
        'studiosite\yii2swipenav\HammerJsAsset',
    ];

    /**
    * Установка альяса при инициализации
    */
    public function init()
    {
        Yii::setAlias('@yii2swipenav', __DIR__);
        parent::init();
    }
}