<?php

namespace studiosite\yii2swipenav;

/**
 * bootstrap nav
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class Nav extends \kartik\nav\NavX
{
	/**
	* @var integer|null Количество отображаемых пунктов (все остальное пойдет в в подпунты итема "еще")
	*/
	public $visibleItems;

	/**
	* @var string Лабел пункты, в который будут добавляться пункты при привышении лимита visibleItems
	*/
	public $moreItemLabel = 'Dropdown';

	/**
	* @var array html параметры подробностей
	*/
	public $moreItemOptions = ['class' => 'more-icon'];

	/**
	* При инициализации, обрезание массива items
	*/
	public function init()
	{
		if (!empty($this->items) && $this->visibleItems) {

			$result = [];
            $chunk = array_chunk($this->items, $this->visibleItems);
            $result = current($chunk);

            if (count($chunk)>1) {
                unset($chunk[0]);
                $subItems = [];
                foreach ($chunk as $value) {
                    $subItems = array_merge($subItems, $value);
                }

                $result[] = ['label' => $this->moreItemLabel, 'items' => $subItems, 'options' => $this->moreItemOptions];
            }

            $this->items = $result;
        }

		parent::init();
	}
}