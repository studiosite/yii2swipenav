
yii2swipenav
=================

Главное меню, с свайпом (бокового меню)

Для свайпа используется [Hammer.js](https://github.com/hammerjs/hammer.js)

Виджет ```Nav``` унаследован от [yii2-nav-x](https://github.com/kartik-v/yii2-nav-x)
## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite/yii2swipenav": "*"
```

в секции ```require``` `composer.json` файла.

## Использование

В отображении

```php
/* @var $this \yii\web\View */
/* @var $content string */

use studiosite\yii2swipenav\Nav;
use studiosite\yii2swipenav\NavBar;

$menuItems = [
    ['label' => 'Link', 'url' => ['/']],
    ['label' => 'Link', 'url' => ['/']],
    ['label' => 'Link', 'url' => ['/']],
    ['label' => 'Link', 'url' => ['/']],
    ['label' => 'Link', 'url' => ['/']],
    '<li class="divider"></li>',
    ['label' => 'Link', 'url' => ['/']],
    ['label' => 'Dropdown', 'url' => ['/'], 'items' => [
        ['label' => 'Link', 'url' => ['/']],
        ['label' => 'Dropdown', 'url' => ['/'], 'items' => [
            ['label' => 'Link', 'url' => ['/']],
            ['label' => 'Link', 'url' => ['/']],
            ['label' => 'Dropdown', 'url' => ['/'], 'items' => [
                ['label' => 'Link', 'url' => ['/']],
                ['label' => 'Link', 'url' => ['/']],
            ]],
        ]],
    ]],
];

echo NavBar::widget([
  'id' => 'mainMenu', // Ид виджета NavBar (Асайд виджет будет иметь id mainMenu_aside)
  'brandLabel' => 'Brand', // Бред
  'brandUrl' => ['site/index'], // Ссылка на бренд
  'swipeOptions' => [ // Опции свайпа
        'selector' => '.wrap', // селектор для обработки жестов
        'enabled' => true // включение отключение инициализации свайп плагина
    	'pluginOptions' => [ // Параметры объекта Hammer @see http://hammerjs.github.io/
          	
		]
  ],
  'nav' => [ // Предустановленный пункт шаблона (конфигурация верхнего Nav виджета и aside Nav, если для aside не установлена конфигурация). Если не указан класс, то для отрисовки будет использован studiosite\yii2swipenav\Nav
    'items' => $menuItems, // Пункты меню (позможно глубокое дерево, за счет наследования Nav от \kartik\nav\NavX)
    'activateParents' => true,
    'encodeLabels' => false,
    'visibleItems' => 3, // Видимые пункты меню, все остальные будут перенесены в dropdown
    'options' => [
      'class' => 'nav navbar-nav'
    ]
  ],
  'options' => [ //  Html Параметры верхнего NavBar
    'class' => 'navbar navbar-default navbar-fixed-top'
  ],
  'template' => '{header}{nav}{search}{login}', // Шаблон содержимого верхнего NavBar
  'templateParams' => [ // Парметры шаблона, будет склеен с дефотными параметрами (nav, header)
    'search' => function($widget) {

      return '<form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                      </form>';
    },
    'login' => function($widget) {

      return Nav::widget([
        'options' => [
          'class' => 'nav navbar-nav navbar-right',
        ],
        'items' => [
          ['label' => 'Link', 'url' => ['/']],
          ['label' => 'Dropdown', 'url' => ['/'], 'items' => [
            ['label' => 'Link', 'url' => ['/']],
          ]]
        ]
      ]);
    }
  ],

  'asideOptions' => [ // Html параметры aside NavBar
    'class' => 'navbar navbar-default',
  ],
  'asideNav' => [ // То же что и nav, только для aside
    'options' => ['class' => 'nav'],
    'items' => $menuItems,
    'activateParents' => true,
    'encodeLabels' => false
  ],
  'asideTemplate' => '{welcome}{header}<div class="clearfix"></div>{nav}', // То же что и template, только для aside
  'asideTemplateParams' => [ // То же что и templateParams, только для aside
    'welcome' => Html::tag('div', 'Welcome', [
      	'class' => 'jumbotron', 
      	'style' => 'padding: 20px; margin: 0px;'
    ])
  ],
]);
```

JS плагин вызывает события. Для обработки:
```php
$this->registerJS("
 	jQuery('#mainMenu_aside').on('opened', function() {
    	console.log('Aside menu is opene');
    });

    jQuery('#mainMenu_aside').on('closed', function() {
         console.log('Aside menu is close');
    });

    jQuery('#mainMenu_aside').on('moving', function() {
         console.log('Aside menu is start moving');
    });             
");
```

#### Исключение элементов от swipe

Указать атрибут data-swipe="0"

```html
<div id="NoSwipeElement" data-swipe="0">
	This unit will not swipe <sub>And this</sub>
</div>
```

