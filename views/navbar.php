<?php

use yii\helpers\Html;
use yii\helpers\Json;
use kartik\icons\Icon;
use studiosite\yii2swipenav\NavAsset;

Icon::map($this, Icon::FA); 

?>

<?= Html::beginTag('nav', $options) ?>
	<div class="container">
		<?= $navContent ?>
	</div>
<?= Html::endTag('nav') ?>


<div class="navbar-toggle-backcloth transition" data-target="#<?= $asideOptions['id'] ?>">
</div>

<?= Html::beginTag('nav', $asideOptions) ?>
	<?= $asideNavContent ?>
<?= Html::endTag('nav') ?>

<div class="container navbar-toggle-container" data-target="#<?= $asideOptions['id'] ?>">
	<button type="button" class="navbar-toggle-button" data-target="#<?= $asideOptions['id'] ?>">
		<span class="sr-only">
	  	Toggle navigation
		</span>
		<?= $toggleButton ?>
	</button>
</div>