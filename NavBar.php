<?php

namespace studiosite\yii2swipenav;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\helpers\Json;
use studiosite\yii2swipenav\NavAsset;

/**
 * Bootstrap NavBar with swipe aside
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class NavBar extends \yii\bootstrap\Widget
{
	/**
	* @var array swipeOptions 
	* @see self::defaultSwipeOptions()
	*/
	public $swipeOptions;

	/**
	* @var string|null Бренд
	*/
	public $brandLabel;

	/**
	* @var array|string Ссылка на бренд
	*/
	public $brandUrl = ['site/index'];

	/**
	* @var array html параметры nav
	*/
	public $options = [];

	/**
	* @var string Шаблон nav
	*/
	public $template = '{header}{nav}';

	/**
	* @var array Параметры виджета Nav, либо конфигурация обекта виджет для создания с помощью createObject
	*/
	public $nav;

	/**
	* @var array Блоки шаблона
	*/
	public $templateParams = [];

	/**
	* @var string Шаблон nav
	*/
	public $asideTemplate = '<div class="clearfix"></div>{header}<div class="clearfix"></div>{nav}';

	/**
	* @var array|null Параметры виджета Nav для aside, либо конфигурация обекта виджет для создания с помощью createObject. По умолчанию == $this->nav
	*/
	public $asideNav;

	/**
	* @var array Блоки шаблона
	*/
	public $asideTemplateParams = [];

	/**
	* @var array html параметры aside
	*/
	public $asideOptions = [];

	/**
	* @var string toggleButton
	*/
	public $toggleButton = '<i class="fa fa-bars"></i>';

	public static function defaultSwipeOptions()
	{
		return [
			'selector' => 'body', // Селектор по которому будут обрабатываться события свайпа
			'enabled' => true, // Включеныа обработка свайпа
			'pluginOptions' => [ // Параметры объекта Hammer @see http://hammerjs.github.io/
				'cssProps' => [
					'userSelect' => true,
				]
			]
		];
	}

	/**
	* Отрисовка виджета
	*/
	public function run()
	{
		$widget = $this;
		$this->asideNav = $this->asideNav ?: $this->nav;

		$defaultTemplateParams = [
			'header' => function () use ($widget) {
				return $widget->renderNavHeader($widget->brandLabel, $widget->brandUrl);
			},
			'nav' => function () use ($widget) {
				return $widget->renderNav($widget->nav);
			}
		];

		$defaultAsideTemplateParams = $defaultTemplateParams;
		$defaultAsideTemplateParams['nav'] = function () use ($widget) {
			return $widget->renderNav($widget->asideNav);
		};

		$templateParams = ArrayHelper::merge($defaultTemplateParams, $this->templateParams);
		$asideTemplateParams = ArrayHelper::merge($defaultAsideTemplateParams, $this->asideTemplateParams);

		$navContent = $this->renderTemplate($this->template, $templateParams);
		$asideNavContent = $this->renderTemplate($this->asideTemplate, $asideTemplateParams);

		$this->swipeOptions = ArrayHelper::merge(self::defaultSwipeOptions(), $this->swipeOptions);

		if (!isset($this->options['id']))
			$this->options['id'] = $this->getId();

		if (!isset($this->options['class']))
			$this->options['class'] = 'header-nav';
		else
			$this->options['class'].= ' header-nav';

		if (!isset($this->asideOptions['id']))
			$this->asideOptions['id'] = $this->getId()."_aside";

		if (!isset($this->asideOptions['class']))
			$this->asideOptions['class'] = 'aside-nav transition';
		else
			$this->asideOptions['class'].= ' aside-nav transition';

		$this->registerClientAssets($this->swipeOptions, $this->asideOptions['id']);

		return $this->render('navbar', [
			'navContent' => $navContent,
			'asideNavContent' => $asideNavContent,
			'options' => $this->options,
			'asideOptions' => $this->asideOptions,
			'toggleButton' => $this->toggleButton,
		]);
	}

	/**
	* Регистрация асетсов
	*
	* @param array $swipeOptions Параметры свайпа
	* @param string $asideId id Aside
	*/
	public function registerClientAssets($swipeOptions, $asideId)
	{
		NavAsset::register($this->view);

		$this->view->registerJS("
			swipeNav.init(".Json::encode($swipeOptions).", '".$asideId."');
		");
	}


	/**
	* Отрисовывает виджет nav
	*
	* @param array $param Параметры указанные в nav или asideNav
	* @return string
	*/
	public function renderNav($params = null)
	{
		if (!is_array($params))
			return '';

		if (isset($params['class'])) {
			$className = $params['class'];
			unset($params['class']);

			return $className::widget($params);
		}

		return Nav::widget($params);
	}

	/**
	* Отрисовывает header navbar
	*
	* @param string|null $brandLabel Бренд
	* @param string|array $brandUrl Ссытка на бред
	* @return string
	*/
	public function renderNavHeader($brandLabel = null, $brandUrl = [])
	{
		if (empty($brandLabel))
			return '';

		return '<div class="navbar-header">
	      <a class="navbar-brand waves-effect" href="'.(is_string($brandUrl) ? $brandUrl : Url::to($brandUrl)).'">'.$brandLabel.'</a>
	    </div>';
	}

	/**
	* Отрисовывает содержимого по шаблону
	*
	* @param string $template Шаблон
	* @param array $params Пункты шаблона
	* @return string
	*/
	public function renderTemplate($template, $params = [])
	{
		$widget = $this;

		return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($params, $widget) {

            $name = $matches[1];
            $value = @$params[$name];

            if (empty($value))
            	return '';

            if (is_callable($value))
            	return $value($widget);

            return $value;

        }, $template);
	}
}