

"use strict";

var swipeNav = {

	/**
	* @var integer Минимальная дистанция для дифа свайпа
	*/
	MIN_DISTANCE: 90,

	/**
	* @vars string Фазы свайпа
	*/
	PHASE_START: 'start',
	PHASE_STOP: 'stop',
	PHASE_MOVE: 'move',

	/**
	* @var integer Текущая позиция (слева) aside
	*/
	asideLeft: 0,

	/**
	* @var integer Начальная позиция (слева) aside
	*/
	asideLeftStart: 0,

	/**
	* @var integer Текущая фаза свайпа
	*/
	phase: 'stop',

	/**
	* @var array Карта отрезков допустимых углов влево вправо [x1, x2]
	*/
	allowAngles: [
		[-30, 30],
		[-180, -150],
		[150, 180],
	],

	/**
	* Инициализация
	*
	* @param array swipeOptions Параметры 
	* @param string asideId Ид асайда 
	*/
	init: function(swipeOptions, asideId) {

		var self = this;
		var useTouch = ('ontouchstart' in window) && swipeOptions.enabled;

		jQuery('.navbar-toggle-container .navbar-toggle-button').click(function() {
			jQuery('#'+asideId).toggleClass('opened');
			if (jQuery('#'+asideId).hasClass('opened')) {
				jQuery('.navbar-toggle-backcloth[data-target="#'+asideId+'"]').addClass('showed');
				jQuery('.navbar-toggle-container[data-target="#'+asideId+'"]').addClass('toggle-close');
				$('#'+asideId).trigger("opened");

			} else {
				jQuery('.navbar-toggle-backcloth[data-target="#'+asideId+'"]').removeClass('showed');
				jQuery('.navbar-toggle-container[data-target="#'+asideId+'"]').removeClass('toggle-close');
				$('#'+asideId).trigger("closed");
			}
		});

		jQuery('.navbar-toggle-backcloth').click(function() {
			jQuery('#'+asideId).removeClass('opened');
			jQuery(this).removeClass('showed');
			jQuery('.navbar-toggle-container[data-target="#'+asideId+'"]').removeClass('toggle-close');
			$('#'+asideId).trigger("closed");
		});

		self.asideLeftStart = self.asideLeft = jQuery('.aside-nav').position().left;

		if (useTouch)
			self.hamerJsInit(swipeOptions, asideId);
	},

	/**
	* Инициализация hammer
	*
	* @param array swipeOptions Параметры 
	* @param string asideId Ид асайда 
	*/
	hamerJsInit: function(swipeOptions, asideId) {

		var selector = jQuery(swipeOptions.selector).length ? jQuery(swipeOptions.selector)[0] : false;

		if (!selector) {

			return console.error('Can not find swipe selector');
		}

		var hammerSelector = new Hammer(selector, swipeOptions.pluginOptions);
		var aside = jQuery('#'+asideId);
		var backcloth = jQuery('.navbar-toggle-backcloth[data-target="#'+asideId+'"]');
		var toggleContainer = jQuery('.navbar-toggle-container[data-target="#'+asideId+'"]');

		hammerSelector.get('pan').set({ 
			direction: Hammer.DIRECTION_HORIZONTAL,
		});

		// Начало свайпа
		var startCallBack = function(event) {

		    var start = event.pointers[0].clientX - event.deltaX;
		    var asideLeft = 0;
	    	var backclothOpacity = 0;

	    	if (!swipeNav.allowAngle(event.angle))
				return true;

	    	if (swipeNav.phase!=swipeNav.PHASE_STOP) {
	    		swipeNav.phase = swipeNav.PHASE_STOP;
	    		
	    		return true;
	    	}

		    if (start>300 && event.direction==Hammer.DIRECTION_RIGHT || $(document).width()>992) {
		    	swipeNav.phase = swipeNav.PHASE_STOP;

		    	return true;
		    }

		    if (jQuery(event.target).data('swipe')===0 || jQuery(event.target).parents('[data-swipe="0"]').length) {
		    	swipeNav.phase = swipeNav.PHASE_STOP;

	    		return true;
		    }
		    aside.removeClass('transition');
			backcloth.removeClass('transition');
			
			swipeNav.asideLeftStart = aside.position().left;
			$('#'+asideId).trigger('moving');

		    aside.removeClass('opened');
		    asideLeft = asideLeft + swipeNav.asideLeft*(-1);
		    swipeNav.phase = swipeNav.PHASE_MOVE;
		    backclothOpacity = (1 - asideLeft/swipeNav.asideLeft)*0.5;

		    backcloth.css({
		       'right': '0px',
		       '-moz-opacity': backclothOpacity,
			   '-khtml-opacity': backclothOpacity,
			   'opacity': backclothOpacity,
		    });
		    backcloth.removeClass('showed');
		    toggleContainer.removeClass('toggle-close');

		    swipeNav.phase = swipeNav.PHASE_START;
		};

		// Движение влево или вправо
		var swipeCallBack = function(event) {

		    var start = event.pointers[0].clientX - event.deltaX;
		    var asideLeft = 0;
	    	var backclothOpacity = 0;
	    	var distance = event.deltaX;
	    	
	    	if (jQuery(event.target).data('swipe')===0) {
	    		swipeNav.phase = swipeNav.PHASE_STOP;

	    		return true;
	    	}

		    if (start>300 && event.type=='panright') {
		    	swipeNav.phase = swipeNav.PHASE_STOP;

		    	return true;
		    }
	    	
		    if (swipeNav.phase!=swipeNav.PHASE_START && swipeNav.phase!=swipeNav.PHASE_MOVE) {

	    		return true;
	    	}

	    	asideLeft = swipeNav.asideLeftStart + distance;

	    	if (asideLeft>=0) 
	    		asideLeft = 0;

	    	if (asideLeft<=swipeNav.asideLeft)
	    		asideLeft = swipeNav.asideLeft;

	    	if (aside.hasClass('transition')) {
	    		aside.removeClass('transition');
	    		backcloth.removeClass('transition');
	    	}

	    	backclothOpacity = (1 - asideLeft/swipeNav.asideLeft)*0.5;
	    	backcloth.css({
		       '-moz-opacity': backclothOpacity,
			   '-khtml-opacity': backclothOpacity,
			   'opacity': backclothOpacity,
			   'right': '0px',
		    });
	    	aside.css({
	    		left: asideLeft+'px'
	    	});

	    	swipeNav.phase = swipeNav.PHASE_MOVE;

	    	return true;
		};

		// Отмена или конец свайпа
		var cancelCallBack = function(event) {
			
			// При проматывании вверх и низ
			if (Math.abs(event.deltaX)<Math.abs(event.deltaY)) {
	    		if (swipeNav.asideLeftStart==0) {
	    			event.deltaX = swipeNav.MIN_DISTANCE + 1;
	    		} else {
	    			event.deltaX = -swipeNav.MIN_DISTANCE - 1;
	    		} 
			}

	    	var asideLeft = 0;

	    	if (swipeNav.phase!=swipeNav.PHASE_MOVE) {
	    		swipeNav.phase = swipeNav.PHASE_STOP;

	    		return true;
	    	}

			if (event.deltaX>swipeNav.MIN_DISTANCE) {
				asideLeft = 0;
			} else if (event.deltaX<-swipeNav.MIN_DISTANCE) {
				asideLeft = swipeNav.asideLeft;
			}

			if (parseInt(aside.css('left'))!=asideLeft) {
				aside.addClass('transition');
				backcloth.addClass('transition');
			}

		    if (asideLeft==0) {
		    	if (!aside.hasClass('opened')) {
			    	aside.addClass('opened');
			    	backcloth.addClass('showed');
			    	toggleContainer.addClass('toggle-close');
			    }

			    $('#'+asideId).trigger('opened');
		    } else {
		    	if (aside.hasClass('opened')) {
			    	aside.removeClass('opened');
			    	backcloth.removeClass('showed');
			    	toggleContainer.removeClass('toggle-close');
		    	}
		    	
		    	$('#'+asideId).trigger('closed');
		    }

		    swipeNav.asideLeftStart = asideLeft;
		    swipeNav.phase = swipeNav.PHASE_STOP;
		    backcloth.attr('style', '');
			aside.attr('style', '');
		};

		hammerSelector.on('panstart', startCallBack);
		hammerSelector.on('panleft panright', swipeCallBack);
		hammerSelector.on('panend pancancel', cancelCallBack);

	},

	

	/**
	* Расчет более точного угла
	* Допускается ли угол
	*
	* @param float angle Угол
	*/
	allowAngle: function(angle) {
		var self = this;
		var allow = false;

		for(var segment in self.allowAngles) {
			if (angle>=self.allowAngles[segment][0] && angle<=self.allowAngles[segment][1]) {
				allow = true;

				break;
			}
		}

		return allow;
	}
};

