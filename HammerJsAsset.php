<?php

namespace studiosite\yii2swipenav;

use Yii;
use yii\web\AssetBundle;

/**
 * Бандл hammer js
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class HammerJsAsset extends AssetBundle
{
	/**
    * @var string Альяс пути где находятся асетсы
    */
	public $baseUrl = '@web';

	/**
    * @var array Исходный путь
    */
    public $sourcePath = '@bower/hammer.js';

	/**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
    ];

	/**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
        'hammer.min.js'
    ];

    /**
    * @var array Список асетсов - зависимости текущего асетса
    */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}